export const npmScrtLinkCore = '[scrt-link-core](https://www.npmjs.com/package/scrt-link-core)'
export const npmScrtLinkCli = '[scrt-link-cli](https://www.npmjs.com/package/scrt-link-cli)'
export const gitlabScrtLink = '[scrt-link](https://gitlab.com/kingchiller/scrt-link)'
export const gitlabScrtLinkCore = '[scrt-link-core](https://gitlab.com/kingchiller/scrt-link-core)'
export const gitlabScrtLinkCli = '[scrt-link-cli](https://gitlab.com/kingchiller/scrt-link-cli)'
export const gitlabScrtLinkSlack =
  '[scrt-link-slack](https://gitlab.com/kingchiller/scrt-link-slack)'
